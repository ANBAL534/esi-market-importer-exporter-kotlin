import net.troja.eve.esi.ApiException
import net.troja.eve.esi.api.MarketApi
import net.troja.eve.esi.api.UniverseApi
import net.troja.eve.esi.model.MarketOrdersResponse

data class Station (val marketApi: MarketApi, val regionId: Int, val stationId: Long) {

    val orders = mutableListOf<MarketOrdersResponse>()
    val stationName = UniverseApi().postUniverseNames(listOf(stationId.toInt()), null)[0].name!!

    fun downloadOrders() {
        // Download station market data
        val stationLists = mutableListOf<List<MarketOrdersResponse>>()
        var stationDownloadPage = 1
        while (true){
            try{
                val marketList = marketApi.getMarketsRegionIdOrders("all", regionId, null, null, stationDownloadPage, null)
                if (!marketList.isEmpty())
                    stationLists.add(marketList)
                else
                    break
                println("[DOWNLOAD] $stationName Page: $stationDownloadPage")
                stationDownloadPage++
            }catch (e: ApiException){
                Thread.sleep(1000)
            }
        }
        println("[DOWNLOAD] $stationName is filtering the orders")
        // Filter all the lists that doesn't are from the selected station
        for (stationList in stationLists)
            for (stationOrder in stationList)
                if (stationOrder.locationId == stationId)
                    orders.add(stationOrder)
        println("[DOWNLOAD] $stationName has finished downloading")
    }

}
import net.troja.eve.esi.ApiException
import net.troja.eve.esi.api.MarketApi
import net.troja.eve.esi.model.MarketStructuresResponse

data class Structure (val marketApi: MarketApi, val structureId: Long, val structureName: String = structureId.toString()) {

    val orders = mutableListOf<MarketStructuresResponse>()

    fun downloadOrders() {
        // Download station market data
        val structureLists = mutableListOf<List<MarketStructuresResponse>>()
        var structureDownloadPage = 1
        while (true){
            try{
                val marketList = marketApi.getMarketsStructuresStructureId(structureId, null, null, structureDownloadPage, null)
                if (!marketList.isEmpty())
                    structureLists.add(marketList)
                else
                    break
                println("[DOWNLOAD] $structureName Page: $structureDownloadPage")
                structureDownloadPage++
            }catch (e: ApiException){
                println(e)
                break
            }
        }
        println("[DOWNLOAD] $structureName is filtering the orders")
        // Filter all the lists that doesn't are from the selected station
        for (stationList in structureLists)
            for (stationOrder in stationList)
                orders.add(stationOrder)
        println("[DOWNLOAD] $structureName has finished downloading")
    }

}
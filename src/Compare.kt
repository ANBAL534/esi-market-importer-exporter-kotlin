import net.troja.eve.esi.ApiException
import net.troja.eve.esi.api.UniverseApi
import net.troja.eve.esi.model.TypeResponse
import kotlin.concurrent.thread
import kotlin.system.exitProcess

object Compare {
    fun compare(orders1: List<BasicOrder>, orders2: List<BasicOrder>, orders1Name: String, orders2Name: String, shipping1to2: Int = 450, shipping2to1: Int = 1350): List<OrderProfit> {
        val threads = mutableListOf<Thread>()

        // Compare
        val orderList = mutableListOf<OrderProfit>()

        val consumedOrders = mutableListOf<BasicOrder>()
        val typeIds = mutableListOf<Int>()
        val profitsPerUnit = mutableListOf<Double>()
        val profitsPerOrder = mutableListOf<Double>()
        val volumeRemains = mutableListOf<Int>()
        val routes = mutableListOf<String>()
        val buy = mutableListOf<Any>()
        val sell = mutableListOf<Any>()
        val itemTypeResponses = mutableMapOf<Int, TypeResponse>()

        for ((indexOrder1, order1) in orders1.withIndex()){
            threads.add(thread(start=true) {
                if (indexOrder1 % 50 == 0)
                    println("[COMPARE] $orders1Name with $orders2Name Order: $indexOrder1 / ${orders1.size}")
                for (order2 in orders2) {
                    if (order2.typeId == order1.typeId && order2 !in consumedOrders) {
                        var m3 = Integer.MAX_VALUE.toFloat()
                        var apiOk = false
                        while (!apiOk) {
                            apiOk = true
                            try {
                                if (order1.typeId in itemTypeResponses) {
                                    m3 = itemTypeResponses.getValue(order1.typeId).packagedVolume
                                }else {
                                    val info = try {
                                        UniverseApi().getUniverseTypesTypeId(order1.typeId, null, null, null, null)
                                    }catch (e: ApiException){
                                        Thread.sleep(500)
                                        apiOk = false
                                        continue
                                    }
                                    itemTypeResponses[order1.typeId] = info
                                    m3 = info!!.packagedVolume
                                }
                            } catch (e: NoSuchElementException) {
                                println(e)
                                exitProcess(0)
                            }
                        }
                        if (!order1.isBuyOrder && order2.isBuyOrder) {
                            // Station -> Structures
                            val shippingCost = m3 * shipping1to2
                            val profit = order2.price - order1.price - shippingCost - (order2.price * 0.015)
                            val lower: Int
                            if (profit > 0 && m3 < 365000) {
                                consumedOrders.add(order2)
                                typeIds.add(order1.typeId)
                                profitsPerUnit.add(profit)
                                lower =
                                        if (order1.volumeRemain <= order2.volumeRemain)
                                            order1.volumeRemain
                                        else
                                            order2.volumeRemain
                                volumeRemains.add(lower)
                                profitsPerOrder.add(lower * profit)
                                routes.add("From $orders1Name to $orders2Name")
                                println("From $orders1Name to $orders2Name")
                                buy.add(order1)
                                sell.add(order2)
                            }
                        } else if (order1.isBuyOrder && !order2.isBuyOrder) {
                            // Structures -> Station
                            val shippingCost = m3 * shipping2to1
                            val profit = order1.price - order2.price - shippingCost - (order1.price * 0.04)
                            val lower: Int
                            if (profit > 0 && m3 < 365000) {
                                consumedOrders.add(order2)
                                typeIds.add(order1.typeId)
                                profitsPerUnit.add(profit)
                                lower =
                                        if (order1.volumeRemain <= order2.volumeRemain)
                                            order1.volumeRemain
                                        else
                                            order2.volumeRemain
                                volumeRemains.add(lower)
                                profitsPerOrder.add(lower * profit)
                                routes.add("From $orders2Name to $orders1Name")
                                println("From $orders2Name to $orders1Name")
                                buy.add(order2)
                                sell.add(order1)
                            }
                        }
                    }
                }
            })
            if (threads.size > 24) {
                for (thread in threads)
                    thread.join()
                threads.removeIf { true }
            }
        }

        println("Mixing results...")
        for ((index, profit) in profitsPerUnit.withIndex())
            orderList.add(OrderProfit(buy, sell, profit, profitsPerOrder[index], volumeRemains[index], routes[index], typeIds[index]))

        return orderList

    }
}
import net.troja.eve.esi.ApiClient
import net.troja.eve.esi.api.MarketApi
import net.troja.eve.esi.api.UniverseApi
import net.troja.eve.esi.auth.OAuth
import java.io.File
import java.text.NumberFormat
import java.time.LocalDateTime
import java.util.*
import kotlin.concurrent.thread
import kotlin.system.exitProcess


val client_id = "31ed2a3ce9c64e07b4cdb751a8edea1f"
val secret_key = "NNa3yDMCaF5cEvvMf6k65xxp0pqYXYu9VQP0hCFI"  // Parece ser que no se usa ya
val refresh_token = "F8QgMrSiV0q2Kw0EN0+00g==" // Usar AuthGenerator.kt para generar un nuevo codigo
val theForge = 10000002
val domain = 10000043
val geminate = 10000029
val jita44 = 60003760L
val amarr = 60008494L
val fdz = 60012730L
val thetastar1dq1 = 1022734985679
var imprialpalace1dq1 = 1030049082711
val `7utb` = 1027266860653
val `j5a-ix` = 1024163597180

fun main(args : Array<String>) {

    val computeComparationStationStation = !args.contains("--skip-station-station")
    val computeComparationStationStructure = !args.contains("--skip-station-structure")
    val computeComparationStructureStructure = !args.contains("--skip-structure-structure")
    val launchBrowser = !args.contains("--no-browser")

    if (args.contains("-h")) {
        println("""
                Usage: java -jar esi-market-importer-exporter-kotlin.jar [options]
                
                Options:
                    --skip-station-station
                    --skip-station-structure
                    --skip-structure-structure
                    --no-browser    This options remove the browser launching when the computation is finished
            """.trimIndent())
        exitProcess(0)
    }

    val apiClient = ApiClient()
    val oauth = apiClient.getAuthentication("evesso") as OAuth
//    oauth.clientId = client_id
//    oauth.refreshToken = refresh_token
    oauth.setAuth(client_id, refresh_token)
    val marketApi = MarketApi()
    marketApi.apiClient = apiClient

    val stations = listOf(Station(marketApi, theForge, jita44), Station(marketApi, domain, amarr))//, Station(marketApi, geminate, fdz))
    val structures = listOf(Structure(marketApi, thetastar1dq1, "1DQ1-thetastar"), Structure(marketApi, imprialpalace1dq1, "1DQ1-ImperialPalace"))//, Structure(marketApi, `7utb`, "7UTB"), Structure(marketApi, `j5a-ix`, "J5A"))
    val orderList = mutableListOf<OrderProfit>()

    // Download orders from stations and structures
    println("Starting market data download")
    val downloadThreads = mutableListOf<Thread>()
    for (station in stations)
        downloadThreads.add(thread(start = true, name = "downloadStation ${station.stationName}") { station.downloadOrders() })
    for (structure in structures)
        downloadThreads.add(thread(start = true, name = "downloadStructure ${structure.structureName}") { structure.downloadOrders() })
    for (thread in downloadThreads)
        thread.join()
    println("Market data download finished")

    // Compare
    // First compare every station with the others
    println("Comparision 1/3 - Station|Station")
    if (stations.isNotEmpty() && computeComparationStationStation) {
        val comparisionThreads = mutableListOf<Thread>()
        var remainingStations = stations
        for (station in stations) {
            comparisionThreads.add(thread {
                remainingStations = remainingStations.filter { it != station }
                remainingStations.forEach {
                    val orders = mutableListOf<BasicOrder>()
                    it.orders.forEach { orders.add(BasicOrder(stationOrder = it)) }
                    val stationOrders = mutableListOf<BasicOrder>()
                    station.orders.forEach { stationOrders.add(BasicOrder(stationOrder = it)) }
                    val comparedOrders = Compare.compare(stationOrders, orders, station.stationName, it.stationName)
                    orderList.addAll(comparedOrders)
                }
            })
        }
        comparisionThreads.forEach { it.join() }
    }
    // Second compare every station with the structures
    println("Comparision 2/3 - Station|Structure")
    if (structures.isNotEmpty() && stations.isNotEmpty() && computeComparationStationStructure) {
        val comparisionThreads = mutableListOf<Thread>()
        for (station in stations) {
            comparisionThreads.add(thread {
                val stationOrders = mutableListOf<BasicOrder>()
                station.orders.forEach { stationOrders.add(BasicOrder(stationOrder = it)) }
                for (structure in structures) {
                    val structureOrders = mutableListOf<BasicOrder>()
                    structure.orders.forEach { structureOrders.add(BasicOrder(structureOrder = it)) }
                    val comparedOrders = Compare.compare(stationOrders, structureOrders, orders1Name = station.stationName, orders2Name = structure.structureName)
                    orderList.addAll(comparedOrders)
                }
            })
        }
        comparisionThreads.forEach { it.join() }
    }
    // Third compare every structure with the others
    println("Comparision 3/3 - Structure|Structure")
    if (structures.isNotEmpty() && computeComparationStructureStructure) {
        val comparisionThreads = mutableListOf<Thread>()
        var remainingStructures = structures
        for (structure in structures) {
            comparisionThreads.add(thread {
                remainingStructures = remainingStructures.filter { it != structure }
                remainingStructures.forEach {
                    val orders = mutableListOf<BasicOrder>()
                    it.orders.forEach { orders.add(BasicOrder(structureOrder = it)) }
                    val stationOrders = mutableListOf<BasicOrder>()
                    structure.orders.forEach { stationOrders.add(BasicOrder(structureOrder = it)) }
                    val comparedOrders = Compare.compare(stationOrders, orders, structure.structureName, it.structureName)
                    orderList.addAll(comparedOrders)
                }
            })
        }
        comparisionThreads.forEach { it.join() }
    }

    // Exchange typeIds for names
    println("Exchanging Ids for Names...")
    val arrayIds = mutableListOf<MutableList<Int>>()
    arrayIds.add(mutableListOf())
    var indexArrayIds = 0
    val addedIds = mutableListOf<Int>()
    for (order in orderList){
        if(order.typeId !in addedIds){
            addedIds.add(order.typeId)
            arrayIds[indexArrayIds].add(order.typeId)
            if(arrayIds[indexArrayIds].size == 1000){
                indexArrayIds++
                arrayIds.add(mutableListOf())
            }
        }
    }

    for (ids in arrayIds){
        val names = UniverseApi().postUniverseNames(ids, null)
        for (name in names){
            for (order in orderList){
                if (order.typeId == name.id){
                    order.name = name.name
                }
            }
        }
    }

    // Calculate averages
    val averageOrders = mutableListOf<OrderProfit>()
    val consumedIds = mutableListOf<Int>()
    for (order in orderList){
        if(order.typeId !in consumedIds){
            consumedIds.add(order.typeId)

            var profitUnitSum = 0.0
            var profitOrderSum = 0.0
            var volumeSum = 0
            var size = 0
            for (order2 in orderList){
                if(order2.typeId == order.typeId && order2.routes == order.routes){
                    profitUnitSum += order2.profitPerUnit
                    profitOrderSum += order2.profitPerOrder
                    volumeSum += order2.volumeRemain
                    size++
                }
            }

            averageOrders.add(OrderProfit(order.buy, order.sell, (profitUnitSum/size), profitOrderSum, volumeSum, order.routes, order.typeId, order.name))
        }
    }

    // Print results and prepare the html
    val script = File("sort_custom_table_profits.js").readText()
    var html = "<script type=\"text/javascript\">$script</script>"
    html += "<html>Updated: <b>${LocalDateTime.now()}</b>"
    html += "<body><table id=\"myTable2\" style=\"width: 100%\">"
    html += "<tr>"
    html += "<th onclick=\"sortCustomTableProfits(0)\">type_id</th>"
    html += "<th onclick=\"sortCustomTableProfits(1)\">name</th>"
    html += "<th onclick=\"sortCustomTableProfits(2)\">order_volume</th>"
    html += "<th onclick=\"sortCustomTableProfits(3)\">unit_profit</th>"
    html += "<th onclick=\"sortCustomTableProfits(4)\">order_profit</th>"
    html += "<th onclick=\"sortCustomTableProfits(5)\">route</th>"
    html += "</tr>"
    println("[RESULTS]")
    averageOrders.sortBy { -it.profitPerOrder }
    for (order in averageOrders){
        println("Check '${order.name}' (${order.typeId}) with a volume of ${order.volumeRemain} and a unit profit of ${NumberFormat.getNumberInstance(Locale.US).format(order.profitPerUnit.toInt())} ISK - ${order.routes}")

        val ceroesUnitProfit = 12-order.profitPerUnit.toInt().toString().length
        var finalUnitProfit = ""
        for (i in 0 until ceroesUnitProfit){
            finalUnitProfit += "0"
        }
        finalUnitProfit += NumberFormat.getNumberInstance(Locale.US).format(order.profitPerUnit.toInt())

        val ceroesOrderProfit = 12-order.profitPerOrder.toInt().toString().length
        var finalOrderProfit = ""
        for (i in 0 until ceroesOrderProfit){
            finalOrderProfit += "0"
        }
        finalOrderProfit += NumberFormat.getNumberInstance(Locale.US).format(order.profitPerOrder.toInt())

        html += "<tr>"
        html += "<td>${order.typeId}</td>"
        html += "<td>${order.name}</td>"
        html += "<td>${order.volumeRemain}</td>"
        html += "<td>$finalUnitProfit</td>"
        html += "<td>$finalOrderProfit</td>"
        html += "<td>${order.routes}</td>"
        html += "</tr>"
    }
    html += "</table></body></html>"

    File("table_results.html").apply {
        writeText(html)
        if (launchBrowser)
            DesktopApi.browse(this.toURI())
    }

}
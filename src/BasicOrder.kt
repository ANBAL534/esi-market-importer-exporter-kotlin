import net.troja.eve.esi.model.MarketOrdersResponse
import net.troja.eve.esi.model.MarketStructuresResponse

data class BasicOrder (val stationOrder: MarketOrdersResponse? = null, val structureOrder: MarketStructuresResponse? = null){
    var typeId: Int =
            if (stationOrder != null)
                stationOrder.typeId
            else
                structureOrder!!.typeId
    var isBuyOrder: Boolean =
            if (stationOrder != null)
                stationOrder.isBuyOrder
            else
                structureOrder!!.isBuyOrder
    var price: Double =
            if (stationOrder != null)
                stationOrder.price
            else
                structureOrder!!.price
    var volumeRemain: Int =
            if (stationOrder != null)
                stationOrder.volumeRemain
            else
                structureOrder!!.volumeRemain
    var locationId: Long =
            if (stationOrder != null)
                stationOrder.locationId
            else
                structureOrder!!.locationId
}
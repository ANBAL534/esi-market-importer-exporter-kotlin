data class OrderProfit(
        val buy: Any,
        val sell: Any,
        val profitPerUnit: Double,
        val profitPerOrder: Double,
        val volumeRemain: Int,
        val routes: String,
        val typeId: Int,
        var name: String = "")
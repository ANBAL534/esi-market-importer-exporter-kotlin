
import net.troja.eve.esi.ApiClient
import net.troja.eve.esi.ApiException
import net.troja.eve.esi.auth.OAuth
import net.troja.eve.esi.auth.SsoScopes
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.io.Reader
import java.net.URI
import java.net.URISyntaxException

/**
 * This main method can be used to generate a refresh token to run the unit
 * tests that need authentication. It is also an example how to use SSO in
 * an implementation.
 *
 * More description is in the README.md
 *
 * @param args
 * The client id.
 * @throws IOException
 * @throws URISyntaxException
 * @throws net.troja.eve.esi.ApiException
 */
@Throws(IOException::class, URISyntaxException::class, ApiException::class)
fun main(args: Array<String>) {
    val client_id = "31ed2a3ce9c64e07b4cdb751a8edea1f"
    val state = "somesecret"
    val client = ApiClient()
    val auth = client.getAuthentication("evesso") as OAuth
    auth.clientId = client_id
    val scopes = SsoScopes.ALL
    val redirectUri = "http://localhost"
    val authorizationUri = auth.getAuthorizationUri(redirectUri, scopes, state)
    println("Authorization URL: $authorizationUri")
    DesktopApi.browse(URI(authorizationUri))

    val br = BufferedReader(InputStreamReader(System.`in`) as Reader?)
    print("Code from Answer: ")
    val code = br.readLine()
    auth.finishFlow(code, state)
    println("Refresh Token: " + auth.refreshToken)
}